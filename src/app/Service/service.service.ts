import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Permiso} from "../Modelo/Permiso";
import {TipoPermiso} from "../Modelo/TipoPermiso";

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http:HttpClient) { }
  UrlGetAll='http://localhost:8080/permiso/getAllPermisos';
  UrlAdd='http://localhost:8080/permiso/addPermiso';
  UrlGetTipoPermisos='http://localhost:8080/permiso/getAllTipoPermiso';
  UrlDeletePermiso='http://localhost:8080/permiso/deletePermiso/';

  getPermisos(){
    return this.http.get<Permiso[]>(this.UrlGetAll)
  }

  addPermiso(permiso:Permiso){
    return this.http.post<Permiso>(this.UrlAdd,permiso);
  }

  getTipoPermisos(){
    return this.http.get<TipoPermiso[]>(this.UrlGetTipoPermisos);
  }

  deletePermiso(id: number | undefined){
    return this.http.delete(this.UrlDeletePermiso+id);
  }
}
