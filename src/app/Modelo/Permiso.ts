export class Permiso{
  id: number | undefined;
  nombreEmpleado:String | undefined;
  apellidosEmpleado:String | undefined;
  fechaPermiso:String | undefined;
  tipoPermiso:String | undefined;
  tipoPermisoId:number | undefined;
  fechaPermisoDate:Date | undefined;
}
