import { Component, OnInit } from '@angular/core';
import {Route, Router} from "@angular/router";
import {ServiceService} from "../../Service/service.service";
import {Permiso} from "../../Modelo/Permiso";
import {TipoPermiso} from "../../Modelo/TipoPermiso";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  permiso: Permiso = new Permiso();
  tipoPermisos: TipoPermiso[] = [];
  permisoForm: FormGroup;


  constructor(private router:Router, private service:ServiceService, private _builder:FormBuilder) {
    this.permisoForm=this._builder.group({
      nombreEmpleado: ['',Validators.required],
      apellidosEmpleado: ['',Validators.required],
      tipoPermiso: ['',Validators.required],
      fechaPermisoDate: ['',Validators.required],
    });

  }

  ngOnInit(): void {
    this.getTipoPermisos();
  }

  Guardar(value: any){
    console.log(value);
    this.permiso.nombreEmpleado = value.nombreEmpleado;
    this.permiso.apellidosEmpleado = value.apellidosEmpleado;
    this.permiso.fechaPermisoDate = value.fechaPermisoDate;
    this.permiso.tipoPermisoId = value.tipoPermiso;
    this.service.addPermiso(this.permiso)
      .subscribe(data=>{
        alert("Permiso agregado con exito")
        this.router.navigate(["listar"])
      })
  }

  getTipoPermisos(){
    this.service.getTipoPermisos()
      .subscribe(data=>{
        this.tipoPermisos=data;
      })
  }

}
