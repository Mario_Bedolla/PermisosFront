import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ServiceService}from '../../Service/service.service'
import {Permiso} from "../../Modelo/Permiso";

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {
  permisosList: Permiso[]=[];

  constructor(private service:ServiceService, private router:Router) { }


  ngOnInit(): void {
    this.getPermisos()
  }

  getPermisos(){
    this.service.getPermisos()
      .subscribe(data=>{
        this.permisosList=data
      })
  }

  Editar(permiso:Permiso):void {
    localStorage.setItem("permiso",JSON.stringify(permiso));
    this.router.navigate(["edit"]);
  }

  Eliminar(permiso:Permiso):void{
    if(window.confirm('¿Estas seguro de eliminar el permiso seleccionado?')){
      this.service.deletePermiso(permiso.id).subscribe();
      let index: number = this.permisosList.indexOf(permiso);
      if (index!==-1){
        this.permisosList.splice(index,1);
      }
      this.router.navigate(["listar"]);
    }
  }
}
