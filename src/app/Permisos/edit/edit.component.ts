import { Component, OnInit } from '@angular/core';
import {Permiso} from "../../Modelo/Permiso";
import {TipoPermiso} from "../../Modelo/TipoPermiso";
import {ServiceService} from "../../Service/service.service";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  permiso: Permiso = new Permiso();
  tipoPermisos: TipoPermiso[] = [];
  permisoForm: FormGroup;

  constructor(private router:Router,private service:ServiceService,private _builder:FormBuilder) {
    this.Editar();
    this.permisoForm=this._builder.group({
      nombreEmpleado: [this.permiso.nombreEmpleado,Validators.required],
      apellidosEmpleado: [this.permiso.apellidosEmpleado,Validators.required],
      tipoPermiso: [this.permiso.tipoPermisoId,Validators.required],
      fechaPermisoDate: [this.permiso.fechaPermiso,Validators.required],
    });
  }

  ngOnInit(): void {
    this.getTipoPermisos();
  }

  Actualizar(permiso: Permiso) {

  }

  Editar(){
    this.permiso = JSON.parse(<string>localStorage.getItem("permiso"));
  }

  getTipoPermisos(){
    this.service.getTipoPermisos()
      .subscribe(data=>{
        this.tipoPermisos=data;
      })
  }

  Guardar(value: any){
    console.log(value);
    this.permiso.nombreEmpleado = value.nombreEmpleado;
    this.permiso.apellidosEmpleado = value.apellidosEmpleado;
    this.permiso.fechaPermisoDate = value.fechaPermisoDate;
    this.permiso.tipoPermisoId = value.tipoPermiso;
    this.service.addPermiso(this.permiso)
      .subscribe(data=>{
        alert("Permiso agregado con exito")
        this.router.navigate(["listar"])
      })
  }
}
